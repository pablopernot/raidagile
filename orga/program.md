

# Formation RAID AGILE 

Une formation différente, dans un environnement dépaysant mais sécurisé, dans une ambiance chaleureuse et décontractée.
Une pédagogie innovante qui vous fournit les outils et surtout, avec la culture, les clés pour accélérer votre transformation agile.

## Audience

Cette formation s’adresse à toutes les personnes qui souhaitent une  mise en oeuvre efficace de l’Agilité  dans leur organisation. Elle accueille les participants individuels (managers, responsables métiers, développeurs, chefs de projet, ainsi que tout acteur impliqué dans le développement ou la maintenance de logiciel) mais aussi des groupes de personnes venant ensemble d’une même organisation.

## Durée de la formation

3 journées de 7 heures chacune soit un total de 21 heures.

## Prérequis

Ouverture d’esprit.

## Objectifs & Impacts de la formation

- Objectif : acquérir un état d’esprit (une culture) "véritablement" agile du développement de produits et services, alignée avec la stratégie au niveau d'une équipe ou d'une organisation.

Impact pour un participant : il sera mieux équipé pour réussir la transformation agile de son organisation.      

- Objectif : apprendre par l’expérience d’experts de l’Agilité, à travers des discussions et des ateliers, dans un cadre différent, dans un endroit calme et propice, avec des pairs venant de différents contextes.

Impact pour un participant : il saura mieux réagir dans les situations où des obstacles apparaissent en ayant gagné de la confiance à résoudre des problèmes couramment rencontrés avec l’Agilité.

- Objectif : expérimenter les techniques qui ont émergé récemment dans l’éco-système de l’Agilité (impact mapping, story mapping, event storming, kanban, affinage du backlog, noEstimates, définition de fini multi-niveaux…) en les pratiquant en groupes.

Impact pour un participant : il aura acquis de nouvelles compétences dans le développement de produits et de services, qu’il sera capable d’appliquer dans son contexte.

## Animateurs

Claude Aubry et Pablo Pernot.


## Résumé de la formation

### Fondations

- Histoire des méthodes agiles (Scrum, XP, Kanban, …)
- Théorie des méthodes agiles
- Application en pratique
- Culture agile, les comportements qu’elle génère

L’apprentissage se fait à travers des présentations, des conversations, des forums ouverts et des ateliers suivis d’une synthèse des animateurs.

### Thèmes avancés
- Intelligence collective dans une équipe Scrum
- Émergence du bon produit avec l’Impact Mapping, le Story Mapping et le Lean Startup
- Affinage du backlog
- Décomposition des stories et des fonctionnalités
- Kanban (portfolio principalement)
- Management 3.0
- Amélioration continue
- Planification, du planning poker au #noEstimates
- Définition de fini multi-niveaux
- Scrum à plusieurs équipes
- Transformation Agile des organisations
- Le modèle Agile Fluency

L’acquisition émerge en participant activement à des ateliers, des évaluations (quiz) individuelles et collectives, suivis de restitutions entre les groupes et d’une conclusion des animateurs.

## Programme de la formation

### Jour 1
- Introduction à l’Agilité avec des exercices de groupe (bâton d’hélium, constellation, les aveugles et l’éléphant)
- Les valeurs et principes du Manifeste Agile
- Scrum dans le mouvement Agile
- La pensée Lean

- La découverte de produit à travers l’Impact Mapping
- Ateliers : des acteurs et des PP, souvenir des impacts, personas
- Rétrospective

### Jour 2
- Quiz interactif sur Scrum suivi d’une discussion et d’une synthèse
- Les pratiques Agiles avec l’atelier la Scierie à Pratiques
- La collaboration avec l’atelier Broken Squares


- Story Mapping
- Backlog
- Planification de release
- Story Dojo
- Autres techniques de rétrospective

### Jour 3
- Quiz interactif sur l’Agilité
- Présentation de Kanban

- Scrum multi-équipes avec l’atelier Puzzle Grand Scrum
- Nouvelles pratiques de rétrospective
- Zoom sur le rôle de Product Owner avec plusieurs équipes
- Transformation Agile avec l’atelier We’re gonna groove
- Présentation et discussion sur la feuille de route élaborée par les participants sur leur transformation agile
