# INFOS 



## Tags pour centraliser les thèmes

- souvenirs
- préparation

## Mettre des images dans le texte

- Utiliser le shortcode figure 

	{{< figure src="/images/3c.png" title="Le guide du raideur" caption="Cliquez dessus ou sur le lien ci-dessous" link="/pdf/raidme.pdf" >}}

## Souci avec le thème Ananke ? 

	hugo mod get 

## Liens 

- Les résultats du formulaire d'inscriptions : https://framaforms.org/node/599180/webform-results