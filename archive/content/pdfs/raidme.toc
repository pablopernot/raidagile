\select@language {french}
\contentsline {section}{\numberline {1}Arrivées \& Départs}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Arrivées}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Départs}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Pendant la formation}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Soirées ?}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Randonnées ?}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Piscine ?}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Programme}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Objectifs \& Impacts de la formation}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Fondations}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Outils \& pratiques}{5}{subsection.3.3}
\contentsline {section}{\numberline {4}Info pratiques}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Gîte}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Repas}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Petit-déjeuner}{6}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Wifi}{6}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Téléphone}{7}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Les draps et les serviettes}{7}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}Eh oui !}{7}{subsection.4.7}
\contentsline {subsection}{\numberline {4.8}Et bien sûr les Cévennes !}{8}{subsection.4.8}
\contentsline {section}{\numberline {5}FAQ}{9}{section.5}
\contentsline {section}{\numberline {6}Le village global}{10}{section.6}
\contentsline {subsection}{\numberline {6.1}Le hashtag !}{10}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Des articles sur le raid}{10}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Des témoignages}{11}{subsection.6.3}
\contentsline {section}{\numberline {7}Contacts}{16}{section.7}
