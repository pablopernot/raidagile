Raiders
###############
:slug: raiders
:date: 2016-01-29
:template: certifications
:subtitle: Les participants du raid : trois journées complètes du petit déjeuner aux soirées en compagnie de Pablo et Claudio, à faire des ateliers, à avoir des conversations et à apprendre de nouvelles pratiques issues des méthodes agiles et au-delà.

.. raw:: html
 
	<a href="#" class="image feature"><img src="/theme/images/raid4/raidagile4-1.jpg" alt="Raider" /></a>


RAID AGILE #1 - Novembre 2014
------------------------------

* Anthony Cassaigne
* Claire Torres
* Cyril Merique
* Goretti Puig
* Jean-Claude Béard
* Maiwenn Leclercq
* Nicolas Jouve 
* Nicolas Montens
* Olivier Peyrusse
* Vincent Renaudie

RAID AGILE #2 - Mars 2015
------------------------------

* Cédric Ménétrier
* Claire Cassan 
* Dani Al Saab
* Didier Plaindoux
* Ghislaine Barbier
* Isabelle Conreaux 
* Jean-François Marronnier
* Joël Conraud 
* Luc Buatois
* Matthieu Dazy 
* Olivier Mariez
* Patxy Gascué
* Pierre-Jean Montroziés
* Serge Prétet 
* Sylvain Thenault 
* Yassine Zakaria 

RAID AGILE #3 - Juin/Juillet 2015
---------------------------------

* Alice Barralon
* Alice Chauvin
* Benjamin Dubois
* Cyrille François
* Elsa Villarubias
* Emmanuel Gringarten
* Eric Michel
* Florent Cayré
* Kamal Hami-Eddine
* Marie-Sophie Michel
* Romain Crunelle
* Sofiene Laadhar
* Sylvie Fabry

RAID AGILE #4 - Janvier 2016
----------------------------

* Adrien Di Mascio
* Caroline Gaulin
* Cyrille Goussard
* David Douard
* Géraldine Legris
* Hervé Leloup
* Jean-Marie Ferrières
* Katia Saurfelt
* Luigi Notarangelo
* Marla Da Silva


RAID AGILE #5 - Avril 2016
--------------------------

* Alexandre Hugot
* Amélie Pléau
* Bruno Leflon
* Emmanuel Labrunye
* Fabien Bosquet
* Hédi Kallel
* Julien Alapetite
* Laura Médioni
* Raouf Abrougui
* Remi Gerbaud
* Richard Cognot

RAID AGILE #6 - Octobre 2016 
----------------------------

* Amaury Daniel
* Bruno Calvet
* Chrystelle Boyet
* Elodie Guillien 
* Fatme El Bsat
* José Delgado Lopez
* Olivier Ropers
* Paul Scandella
* Quentin Cavigneaux
* Yan Voté

RAID AGILE #7 - Mai 2017 
------------------------

* Capucine Laverrière-Duclos
* François Terrier
* Isabelle Klein-Esnault 
* Jean-Christophe Arraou
* Michel Javaux 
* Nicolas Beladen 
* Nicolas Cherpeau 
* Olivier Grosse
* Régis De Porre
* Romain Merland 
* Sabrina Lefevre
* Sylvain Côte
* Sophie Sy-Yin
* Thomas Blaise
* Wan-Chiu Li 

RAID AGILE #8 - Octobre 2017 
----------------------------

* Arnaud Gonzales
* Denis Benoist
* Eric Greven
* Fabien Berger
* Florence Marguet
* Gauthier Passard
* Jean Palazuelos
* Jean-Pascal Boignard
* Rémi Cretet
* Thomas Leclerc

RAID AGILE #9 - Juin 2018 
----------------------------

* David Desfougères
* Dominique Ribouchon
* Itzel Sanchez-Dehesa
* Frank Bessou
* Jean-Marie Peschoux
* Maël Vala-Viaux
* Noëllie Pilandon
* Simon Rodriguez
* Yannick Franck Hazoume
 

104 raiders plus les 19 du raid tribal. 9 raids classiques, 1 raid tribal. 123 raiders de 2014 à 2018.   