Raid2
######
:slug: raid2
:date: 2014-02-02
:template: raid2
:maintitle: RAID AGILE #2
:subtitle: Quelques souvenirs du **raid agile #2**, mars 2015

.. raw:: html

	<section class="wrapper style2 container special">
	<div class="12u">
	<header>
	<h2>Articles</h2> 
	</header>
				

`Raid Agile #3C et Raid Agile #4Q par Claude Aubry`_	

`Festival de rétrospectives par Pablo Pernot`_

`De retour du raid agile par Sylvain Thenault`_

.. _`Raid Agile #3C et Raid Agile #4Q par Claude Aubry`: http://www.aubryconseil.com/post/Raid-Agile-3C-et-Raid-Agile-4Q
.. _`Festival de rétrospectives par Pablo Pernot`: `Festival de rétrospectives par Pablo Pernot`

.. _`De retour du raid agile par Sylvain Thenault`: https://www.logilab.org/288475

.. raw:: html
	
	</div></section>



.. raw:: html 

	<section class="wrapper style1 container special-alt">
	<div class="row half">	
	<div class="12u">


Témoignages
===========


"J’ai adoré le Raid, c’est sans conteste une des meilleures formations que j’ai eues en quatorze ans de boutique, et une des rares dont je sois reparti avec quelques idées à tenter à court terme, en plus d’un bon coup de culture générale et d’une vision plus claire de ce qu’on peut espérer (ou pas) de l’agilité. Le changement de cadre et la vie en commun pendant ces trois jours me semblent être la bonne formule pour cette formation vu qu’il est – précisément – question de (re)mettre l’humain et la communication directe au cœur du système.

 
Le contenu était dense mais plutôt bien équilibré et privilégiait la mise en application directe et l’alternance avec des temps de discussion libre, et je pense qu’on a vu des choses dont la richesse se révèlera avec le temps. Avoir deux intervenants qui ne sont pas d’accord en tout est un vrai plus, évite une approche dogmatique et a bien contribué à démystifier et désacraliser l’univers agile. Contrairement à ce que je redoutais en arrivant, avoir deux tiers de participants venant de la même entreprise n’a pas du tout déséquilibré les débats et on a eu des échanges très riches avec tout le monde.

 
Cerise sur le gâteau, bonne ambiance, bonne chère (prévoyez un régime avant et après), beau temps, banjo au petit déj’ et jeux de société le soir.

 
Si la question *agile* vous intéresse, n’hésitez pas !"


-- **Matthieu Dazy**


.. raw:: html

	<hr/>

"I just came back from the best Agile workshop ever! A 3-days bootcamp, dynamic, fun, packed with information. My head is full of plans to put it to good use, my legs are sore from hiking, and I probably gained 2 to 4 pounds in the process. A great place to meet new people, get to know your colleagues better, and improve your agile skills. It was AWESOME!" (see the post on linkedin_).

-- **Claire Cassan**

.. _linkedin: https://www.linkedin.com/pulse/raid-agile-claire-cassan

.. raw:: html

	<hr/>

"Merci pour ce raid qui a dépassé toutes nos espérances à tout point de vue : ambiance, formation, randos ... et nourriture !"
 
-- **Ghis**
 

.. raw:: html

	<hr/>

"Pour rester ou devenir Agile, de corps, d'esprit et pourquoi pas de cœur, rien ne vaut le Raid Agile proposé par Clodio et Pablo."

-- **Serge Prétet**

.. raw:: html 

	</div></div></section>

