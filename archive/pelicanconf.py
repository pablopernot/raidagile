#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Clodio & Pablo'
SITENAME = u'Raid Agile'
SITEURL = 'http://raidagile.fr'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

###PABLO
STATIC_PATHS = ['font', 'js', 'css', 'images']
THEME = 'raidagiletheme'
PAGE_PATHS=['']
###
LOAD_CONTENT_CACHE = False
TYPOGRIFY = True


# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
