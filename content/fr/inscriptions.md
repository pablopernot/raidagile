---
title: S'inscrire au raid agile !
featured_image: '/images/photos/2017-10/raid8-1.jpg'
omit_header_text: false
description: Inscrivez-vous !
type: page
---

**Pour vous inscrire au 10ème raid agile en novembre 2022 c'est ici**

(si le formulaire ne s'affiche pas, ou pour plus de confort : [accès direct au formulaire d'inscription](https://framaforms.org/inscription-au-raid-agile-10-nov-2022-1659443614))

{{< form-framaform >}}

Les données collectées nous servent uniquement à vous répondre et à communiquer avec vous pour l'inscription.
