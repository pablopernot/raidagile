---
title: "RAID AGILE EN CÉVENNES"
description: "Édition 2023"
cascade:
  featured_image: '/images/cevennes.jpg'
---

### Pourquoi venir au Raid ?

- Une formation différente, dans un environnement dépaysant mais sécurisé, dans une ambiance chaleureuse et décontractée.
- Une pédagogie innovante qui fournit, avec les outils et surtout la culture, les clés pour accélérer votre appropriation agile.

### En quoi est-ce différent ?

Le **Raid Agile, c’est un groupe de personnes** qui se retrouvent dans les Cévennes avec [trois animateurs](/post/2022-06-03-equipe-raid/). Pendant trois jours et trois nuits, elles partagent leurs expériences, jouent ensemble, goûtent à des outils d’intelligence collective, randonnent dans les Cévennes, mangent et boivent des spécialités locales. Cette ambiance a prouvé ouvrir l’esprit des participants et accroitre leurs capacités dans les nouvelles formes d’organisation du travail.

### Quand ?

Quand il y aura assez de participants — environ une dizaine — pour que ce soit viable. Nous prévoyons que ce sera au début du printemps 2023. Le Raid commence un lundi (ou mardi) midi par le déjeuner et se finit le jeudi (ou vendredi) matin par le petit-déjeuner.

### Où ?

Au grand gîte du [Mas du Canton](https://www.grand-gite-gard-cevennes-sud.com/), dans les Cévennes, entre Lasalle et Saint-Jean du Gard, où nous serons hébergés. Nous mangerons ensemble la nourriture raffinée [des p'tis plats de Julie](https://www.lesptitsplatsdejulie.fr/).

Nous assurons le transfert en van depuis la gare TGV de Nîmes.

### Comment s'inscrire ?

Pas d'inscription officielle ouverte pour l'instant, nous attendons d'avoir suffisamment de participants. Utilisez ce formulaire de [contact](/contact/) pour lancer l'échange avec nous, nous donnez vos attentes, avancer sur les dates, discuter du contenu.

Vous pouvez lire d'abord la [FAQ](/post/2018-la-faq/) ou le [guide du raideur](/post/2022-guide-du-raideur/) au cas où.  
