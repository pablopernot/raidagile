---
title: Contacter le raid agile !
featured_image: '/images/cevennes.jpg'
omit_header_text: false
description: Laissez-nous un message.
type: page
---



Pour nous joindre : 


Soit 

- Claude Aubry chez mastodon : @claudeaubry@mamot.fr
- Pablo Pernot chez mastodon : @pablopernot@toot.portes-imaginaire.org

Sinon sur nos blogs respectifs 

- Claude aubry : https://claudeaubry.fr/
- Pablo Pernot : https://pablopernot.fr

Merci 