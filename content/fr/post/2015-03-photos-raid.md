---
date: 2015-03-01
description: "Photos du raid de mars 2015"
aliases : "/raid2.html"
featured_image: "/images/photos/2015-03/image1.jpg"
tags: ["souvenirs"]
title: "Raid Agile #2, mars 2015, les photos"
---

Les photos du raid agile de mars 2015.


{{< figure src="/images/photos/2015-03/image1.jpg" caption="La quête du sens" >}}
{{< figure src="/images/photos/2015-03/image2.jpg" caption="Un groupe studieux" >}}
{{< figure src="/images/photos/2015-03/image3.jpg" caption="Prendre des décisions" >}}
{{< figure src="/images/photos/2015-03/image4.jpg" caption="Des jeux et du vin" >}}
{{< figure src="/images/photos/2015-03/image5.jpg" caption="Par là" >}}
{{< figure src="/images/photos/2015-03/image6.jpg" caption="Quiz sur le Mont Brion" >}}
{{< figure src="/images/photos/2015-03/image7.jpg" caption="Ça bosse dur" >}}
{{< figure src="/images/photos/2015-03/image8.jpg" caption="Des ateliers agiles" >}}
{{< figure src="/images/photos/2015-03/image9.jpg" caption="Des balades et randonnées" >}}
{{< figure src="/images/photos/2015-03/image10.jpg" caption="Petit déjeuner au soleil (non garanti)" >}}
