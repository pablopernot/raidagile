---
date: 2022-06-01
description: "Guide du raideur"
featured_image: "/images/photos/2018-06/raid9-9.jpg"
tags: ["préparation"]
title: "Le guide du raideur"
---

Un guide tout en agilité, qui vous dit comment vous préparez au Raid !

<!--more-->

{{< figure src="/images/3c.png" title="Le guide du raideur" caption="Cliquez dessus ou sur le lien ci-dessous" link="/pdf/raidme.pdf" >}}

[Le guide du raideur](/pdf/raidme.pdf)
