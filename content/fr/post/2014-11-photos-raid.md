---
date: 2014-11-01
description: "Photos du raid de novembre 2014"
aliases : "/raid1.html"
featured_image: "/images/photos/2014-11/image1.jpg"
tags: ["souvenirs"]
title: "Raid Agile #1, novembre 2014, les photos"
---

Les photos du raid agile de novembre 2014.

{{< figure src="/images/photos/2014-11/image1.jpg" caption="Un paysage inspirant" >}}
{{< figure src="/images/photos/2014-11/image2.jpg" caption="Des repas caloriques" >}}
{{< figure src="/images/photos/2014-11/image3.jpg" caption="Le gîte du canton" >}}
{{< figure src="/images/photos/2014-11/image4.jpg" caption="Le gîte du canton encore" >}}
{{< figure src="/images/photos/2014-11/image5.jpg" caption="Nourriture intellectuelle" >}}
{{< figure src="/images/photos/2014-11/image6.jpg" caption="Rétrospective châtaigne" >}}
{{< figure src="/images/photos/2014-11/image7.jpg" caption="Soleil (non garanti)" >}}
{{< figure src="/images/photos/2014-11/image8.jpg" caption="Des randonnées" >}}
{{< figure src="/images/photos/2014-11/image9.jpg" caption="Encore du sport" >}}
