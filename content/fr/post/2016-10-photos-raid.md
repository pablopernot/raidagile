---
date: 2016-10-01
description: "Photos du raid de octobre 2016"
aliases : "/raid6.html"
featured_image: "/images/photos/2016-10/raid6-4.jpg"
tags: ["souvenirs"]
title: "Raid Agile #6, octobre 2016, les photos"
---

Les photos du raid agile de octobre 2016.


{{< figure src="/images/photos/2016-10/raid6-1.jpg" caption="Estimation..." >}}
{{< figure src="/images/photos/2016-10/raid6-2.jpg" caption="Les cuisses chauffent" >}}
{{< figure src="/images/photos/2016-10/raid6-3.jpg" caption="Les esprits chauffent" >}}
{{< figure src="/images/photos/2016-10/raid6-4.jpg" caption="Prêt pour la valeur ?" >}}
{{< figure src="/images/photos/2016-10/raid6-5.jpg" caption="Christophe & Pablo" >}}
{{< figure src="/images/photos/2016-10/raid6-6.jpg" caption="Soleil (non garanti)" >}}
{{< figure src="/images/photos/2016-10/raid6-7.jpg" caption="Franchement je ne sais plus..." >}}
{{< figure src="/images/photos/2016-10/raid6-8.jpg" caption="Les jeux du soir" >}}
{{< figure src="/images/photos/2016-10/raid6-9.jpg" caption="Le groupe du raid #6" >}}
{{< figure src="/images/photos/2016-10/raid6-10.jpg" caption="Détente" >}}
{{< figure src="/images/photos/2016-10/raid6-11.jpg" caption="Quiz en balade" >}}
