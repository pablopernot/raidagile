---
date: 2017-10-01
description: "Photos du raid de octobre 2017"
aliases : "/raid8.html"
featured_image: "/images/photos/2017-10/raid8-1.jpg"
tags: ["souvenirs"]
title: "Raid Agile #8, octobre 2017, les photos"
---


{{< figure src="/images/photos/2017-10/raid8-1.jpg" caption="L'appel de la nature" >}}
{{< figure src="/images/photos/2017-10/raid8-2.jpg" caption="Les feuilles mortes" >}}
{{< figure src="/images/photos/2017-10/raid8-3.jpg" caption="Laurent Morisseau et les raiders" >}}
{{< figure src="/images/photos/2017-10/raid8-4.jpg" caption="Les Cévennes" >}}
{{< figure src="/images/photos/2017-10/raid8-5.jpg" caption="Soleil sur les brainstorming" >}}
{{< figure src="/images/photos/2017-10/raid8-6.jpg" caption="Pétanque et piscine" >}}
{{< figure src="/images/photos/2017-10/raid8-7.jpg" caption="Kanban ?" >}}
{{< figure src="/images/photos/2017-10/raid8-8.jpg" caption="Relax" >}}
{{< figure src="/images/photos/2017-10/raid8-9.jpg" caption="Découper les histoires" >}}
{{< figure src="/images/photos/2017-10/raid8-10.jpg" caption="Le groupe octobre 2017" >}}
