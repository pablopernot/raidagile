---
date: 2022-09-23
description: "Nouvelle formule"
featured_image: "/images/photos/2016-10/raid6-3.jpg"
tags: ["préparation"]
title: "Nouvelle formule"
---

Quand nous avons décidé de relancer le Raid Agile, nous avons programmé le premier en novembre 2022. Mais nous sommes un mois et demi avant et il n'y a pas suffisamment d'inscrits. Nous devons donc l'annuler.

C'est un échec.

Mais…

<!--more-->
Il a toujours été difficile d'organiser un Raid Agile, c'est d'ailleurs la raison principale pour laquelle nous avions arrêté.

Cette fois, comme d'autres avant, nous avons essayé de fixer une date à l'avance. Plusieurs personnes nous ont dit qu'elles voulaient venir mais que la date ne leur convenait pas, d'autres se sont inscrites et ont finalement annulé.

En conséquence, pour rebondir après l'échec, nous changeons de formule d'inscription.

Nous allons attendre d'avoir suffisamment de personnes intéressées avant de choisir - ensemble — la date qui va bien.
Donc pour l'instant, nous ne demandons pas d'inscription officielle. Nous proposons de lancer la conversation, d'abord avec nous, utilisez twitter ou nos contacts linkedin.

Nous espérons que cela nous permettra de faire ce Raid Agile au printemps 2023, environ.
