---
date: 2015-06-01
description: "Photos du raid de juin/juillet 2015"
aliases : "/raid3.html"
featured_image: "/images/photos/2015-06/raid3-1.jpg"
tags: ["souvenirs"]
title: "Raid Agile #3, juin/juillet 2015, les photos"
---

Les photos du raid agile de juin/juillet 2015.


{{< figure src="/images/photos/2015-06/raid3-1.jpg" caption="Constellation pour mieux se connaitre" >}}
{{< figure src="/images/photos/2015-06/raid3-2.jpg" caption="On coupe là pour le minimum viable" >}}
{{< figure src="/images/photos/2015-06/raid3-3.jpg" caption="Au raid c'est #noguru" >}}
{{< figure src="/images/photos/2015-06/raid3-4.jpg" caption="Daily Scrum à la piscine" >}}
{{< figure src="/images/photos/2015-06/raid3-5.jpg" caption="Notes collantes sur bord de piscine" >}}
{{< figure src="/images/photos/2015-06/raid3-6.jpg" caption="Persona" >}}
{{< figure src="/images/photos/2015-06/raid3-7.jpg" caption="Story mapping" >}}
{{< figure src="/images/photos/2015-06/raid3-8.jpg" caption="Impact mapping" >}}
{{< figure src="/images/photos/2015-06/raid3-9.jpg" caption="Un invité prestigieux" >}}
{{< figure src="/images/photos/2015-06/raid3-10.jpg" caption="Tu la tires ou tu la pointes ?" >}}
{{< figure src="/images/photos/2015-06/sketch-cyrille-j1.jpg" caption="Sketch de Cyrille J1" >}}
{{< figure src="/images/photos/2015-06/sketch-cyrille-j1-2.jpg" caption="Sketch de Cyrille J1-2" >}}
{{< figure src="/images/photos/2015-06/sketch-cyrille-j2.jpg" caption="Sketch de Cyrille J2" >}}
{{< figure src="/images/photos/2015-06/sketch-cyrille-j3.jpg" caption="Sketch de Cyrille J3" >}}
