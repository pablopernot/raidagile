---
date: 2016-01-01
description: "Photos du raid de janvier 2016"
aliases : "/raid4.html"
featured_image: "/images/photos/2016-01/raidagile4-1.jpg"
tags: ["souvenirs"]
title: "Raid Agile #4, janvier 2016, les photos"
---

Les photos du raid agile de janvier 2016.

{{< figure src="/images/photos/2016-01/raidagile4-1.jpg" caption="Des balades, randonnées" >}}
{{< figure src="/images/photos/2016-01/raidagile4-2.jpg" caption="Prendre des décisions" >}}
{{< figure src="/images/photos/2016-01/raidagile4-3.jpg" caption="Dynamique de groupe dans la mousse verte" >}}
{{< figure src="/images/photos/2016-01/raidagile4-4.jpg" caption="Par là..." >}}
{{< figure src="/images/photos/2016-01/raidagile4-5.jpg" caption="Parlez-vous couramment agile ?" >}}
{{< figure src="/images/photos/2016-01/raidagile4-6.jpg" caption="Agile guilleret" >}}
{{< figure src="/images/photos/2016-01/raidagile4-7.jpg" caption="Le petit coin du feu" >}}
{{< figure src="/images/photos/2016-01/raidagile4-8.jpg" caption="La quête du sens" >}}
{{< figure src="/images/photos/2016-01/raidagile4-9.jpg" caption="Un groupe studieux" >}}
{{< figure src="/images/photos/2016-01/raidagile4-10.jpg" caption="On est tous ravi de participer" >}}
