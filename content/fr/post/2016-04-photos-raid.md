---
date: 2016-04-01
description: "Photos du raid de avril 2016"
aliases : "/raid5.html"
featured_image: "/images/photos/2016-04/raidagile5-2.jpg"
tags: ["souvenirs"]
title: "Raid Agile #5, octobre 2016, les photos"
---

Les photos du raid agile de avril 2016.

{{< figure src="/images/photos/2016-04/raidagile5-1.jpg" caption="Des balades, randonnées" >}}
{{< figure src="/images/photos/2016-04/raidagile5-2.jpg" caption="Des risques inconsidérés" >}}
{{< figure src="/images/photos/2016-04/raidagile5-3.jpg" caption="Un vrai YALTA Agile" >}}
{{< figure src="/images/photos/2016-04/raidagile5-4.jpg" caption="Twilight dans les Cévennes" >}}
{{< figure src="/images/photos/2016-04/raidagile5-5.jpg" caption="Eazy raiders" >}}
{{< figure src="/images/photos/2016-04/raidagile5-6.jpg" caption="Orange is the new" >}}
{{< figure src="/images/photos/2016-04/raidagile5-7.jpg" caption="Le gîte" >}}
{{< figure src="/images/photos/2016-04/raidagile5-8.jpg" caption="Ça groove" >}}
{{< figure src="/images/photos/2016-04/raidagile5-9.jpg" caption="Étalon maître pour les estimations" >}}
{{< figure src="/images/photos/2016-04/raidagile5-10.jpg" caption="Mont Brion" >}}
