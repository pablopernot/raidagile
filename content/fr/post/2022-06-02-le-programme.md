---
date: 2022-07-21
description: "Le programme"
featured_image: "/images/photos/2017-10/raid8-1.jpg"
tags: ["préparation"]
title: "Le programme"
---

Le Raid agile est agile dans son contenu, relatif à l'agilité dans toute sa diversité — sans oublier les fondamentaux — et agile dans son déroulement qui n'est pas figé dans le marbre.

<!--more-->

## Audience

Cette formation s’adresse à toutes les personnes qui souhaitent une mise en oeuvre efficace de l’agilité dans leur organisation. Elle accueille les participants individuels (managers, responsables métiers, développeurs, coachs, facilitateurs) mais aussi des groupes de personnes venant ensemble d’une même organisation.

## Durée de la formation

3 journées de 7 heures chacune soit un total de 21 heures.

## Prérequis

Ouverture d’esprit.

## Objectifs & Impacts de la formation

### Fondamentaux de l'agilité

- Objectif : acquérir les fondamentaux : les outils et l'état d’esprit (la culture) agile du développement de produits et services, au niveau d'une équipe ou d'une organisation.
- Impact pour un participant : il sera mieux équipé pour réussir la transformation agile de son organisation.  

### Nouvelles capacités    

- Objectif : apprendre par l’expérience auprès d’experts de l’agilité, à travers des discussions et des ateliers, dans un cadre différent, dans un endroit calme et propice, avec des pairs venant de différents contextes.
- Impact pour un participant : il saura mieux réagir dans les situations où des obstacles apparaissent en ayant gagné de la confiance à résoudre des problèmes couramment rencontrés avec l’agilité.

### Nouvelles compétences
- Objectif : expérimenter les techniques qui ont émergé récemment dans l’éco-système de l’Agilité (impact mapping, story mapping, event storming, kanban, affinage du backlog, noEstimates, définition de fini multi-niveaux…) en les pratiquant en groupes.
- Impact pour un participant : il aura acquis de nouvelles compétences dans le développement de produits et de services, qu’il sera capable d’appliquer dans son contexte.

## Animateurs

Claude Aubry, Dragos Dreptate et Pablo Pernot.

## Résumé de la formation

### Fondamentaux

- Histoire des méthodes agiles (Scrum, XP, Kanban, …)
- Théorie des méthodes agiles
- Application en pratique
- Culture agile, les comportements qu’elle génère
- Les 2 aspects clés : l'équipe auto-organisée et les boucles de feedback.

L’apprentissage se fait à travers des présentations, des conversations, des forums ouverts et des ateliers suivis d’une synthèse des animateurs.

### Thèmes avancés

L'ordre dans lequel ces sujets sont abordés est défini par les animateurs et les participants.

- Intelligence collective dans une équipe : prises de décision, gestion des conflits
- Émergence du bon produit avec l’Impact Mapping, le Story Mapping et le Lean Startup
- Décomposition des stories et des fonctionnalités
- Kanban (portfolio principalement)
- Management 3.0
- Amélioration continue
- Planification, du planning poker au #noEstimates
- Agilité à l'échelle
- Transformation Agile des organisations
- Le modèle Agile Fluency
- Place des OKRs dans la stratégie
- Le démarrage avec la charte projet
- La prise en compte des nouveaux enjeux : éco-responsabilité, planification, télétravail.


L’acquisition émerge en participant activement à des ateliers, des évaluations (quiz) individuelles et collectives, suivis de restitutions entre les groupes et d’une conclusion des animateurs.

{{< figure src="/images/photos/2018-06/raid9-12.jpg" title="Réflexions au mont Brion sur les fondations de l'agilité" >}}
