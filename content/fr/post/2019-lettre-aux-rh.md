---
date: 2022-06-20
description: "Lettre aux RH"
featured_image: "/images/photos/2016-04/raidagile5-3.jpg"
tags: ["préparation"]
title: "Lettre aux RH"
---

> La chose la plus importante en communication, c'est d'entendre ce qui n'est pas dit. -- Peter Drucker

Il y a quelques années, nous avions reçu un message qui, avouons-le, nous avait surpris.

Voici l'histoire, qui finit bien.<!--more-->

## Le message en question 	

---

Je gère une petite équipe de dév [...] et une personne de mon équipe vient d’être « promue » Product Manager/Product Owner.

Elle m’a demandé de participer, dans le cadre de sa formation à votre Raid Agile. Je n’y vois personnellement pas d’inconvénients, mais nos RH sont un peu plus dubitatif car votre site ne les a pas mis en confiance, notamment la partie « Balades et repas en commun. Tout pour se retrouver et se ressourcer, se découvrir et s’aligner sur des valeurs essentielles. Avoir des conversations hors du temps. » (et ils doivent approuver toutes les formations).

J’aurais donc besoin, pour appuyer cette demande de quelques éléments de réponse.

Pouvez-vous me confirmer que les balades etc. se font en dehors de la formation et que les 7 heures de formation par jour ont bel et bien un contenu pédagogique ?

Le tarif comprend il aussi le l’hébergement (nuitées et repas)?

Si vous avez d’autres éléments pouvant appuyer le caractère « professionnel » de la formation, je suis preneur !

Merci d’avance pour vos lumières.

---

Si vos RH ou vos responsables formation se posent ce genre de questions, voici les éléments de réponse que nous avons donnés à cette demande, actualisés pour l'édition 2022 :

## Notre lettre au RH

### Une vraie formation

Nous ne sommes pas une secte. Les animateurs ne sont pas des gourous, ni des coachs de vie. Il ne s’agit pas d’une formation _new age_ d’épanouissement personnel.

Ils ont une longue expérience de l'agilité dans les projets IT et ont publié des choses sérieuses, comme par exemple ces quelques livres :

{{< figure src="/images/livresRaid.png" title="Les livres des animateurs du Raid" alt="Livres sur Scrum, Kanban et l'agilité" >}}


Le Raid a le qualificatif d’Agile en référence à un mouvement issu du développement de logiciel, dont les valeurs sont portées par un Manifeste : http://www.agilemanifesto.org.

Le contenu pédagogique s’appuie sur du matériel issu de ce mouvement agile, très vivant et innovant, d’où le style différent. Il est très riche, on en a une (petite) idée avec le programme.

### Modalités

Oui, les repas et les nuitées sont compris dans le prix, et même le trajet à partir de la gare TGV de Nîmes.  

Nous accompagnons les participants de 8h30 le matin après le petit déjeuner jusqu’à généralement 21h30 le soir avec des pauses dans la journée (1h20mn) et pendant les deux repas.

La balade comprend des ateliers dédiés, c'est donc des éléments importants dans notre parcours. Les participants sont libres de continuer la soirée comme ils l’entendent.

### Une formation professionnelle

Ce qui compte pour nous, ce sont les résultats à l’issue de la formation, la valeur apportée aux participants. De ce point de vue là, nous sommes très satisfaits et les participants aussi, ils nous le disent [dans leur témoignages](/post/temoignages/).

L’apprentissage se fait par des ateliers en groupe, dont on sait que c’est la meilleure pédagogie pour acquérir de nouvelles capacités et pouvoir les appliquer dans son entreprise.

Des entreprises nous font confiance en envoyant plusieurs participants ou en revenant plusieurs fois avec de nouveaux participants : Intel Toulouse (qui a d’ailleurs mis en avant le Raid Agile dans des présentations publiques), GrDF, Logilab, Eliocity, Paradigm, IBM, FDJ, etc.

### Culture d'entreprise et RH

Enfin, les RH sont des acteurs importants de la culture d’entreprise : ils sont donc les bienvenus au Raid Agile ; ils trouveraient beaucoup de matière dans notre formation en tant que participants. Nous espérons donc les voir s’inscrire. Mais même s’ils viennent en nombre, il y aura quand même du banjo et des conversations sur les chemins cévenols.

## Happy end

À l'époque, notre réponse n'avait pas suffi à convaincre cette RH en question d'autoriser la participation au Raid Agile.

Mais, bonne nouvelle, la personne qui avait fait la demande initiale — et qui a depuis changé d'entreprise — va participer à l'édition 2022.

{{< figure src="/images/photos/2018-06/raid9-14.jpg" title="" >}}
