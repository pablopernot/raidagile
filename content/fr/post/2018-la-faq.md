---
date: 2022-06-01
description: "Foire aux questions"
aliases : "/faq.html"
featured_image: "/images/photos/2018-06/raid9-10.jpg"
tags: ["préparation"]
title: "Foire aux questions"
---

Les réponses aux questions fréquemment posées.

<!--more-->

### Est-ce que j'aurai une chambre "tranquille" ?

Dans le gîte des Cévennes vous aurez une chambre individuelle ou à deux avec une douche et des toilettes indépendantes (dans la chambre) pour être à l'aise.

### Je ne veux pas faire les randonnées, le raid agile c'est foutu pour moi ?

Non, le raid agile dans les Cévennes c'est surtout une équipe isolée dans un lieu particulier. Les randonnées sont tranquilles et ne constituent pas une grande partie du temps (2 ou 3 fois 2h).
Et personne n'est obligé d'y participer. 

### J'aime bien être en groupe, mais il faut que je m'isole de temps en temps, c'est possible ?

Oui, il y a des moments de "quartier libre" pendant le raid. Par exemple avant de reprendre l'après-midi, du temps est laissé à ceux qui aiment faire la sieste. Le soir, nous proposons des jeux, c'est une invitation, viennent ceux qui ont envie et on peut arrêter quand on veut.

### Je ne suis pas informaticien encore moins geek et je ne connais pas de méthode agile, je peux venir quand même ?

Si le Manifeste Agile, en 2001 s’adressait principalement aux développeurs de logiciel, le Raid Agile accueille tous ceux qui veulent s’ouvrir l’esprit à l’innovation, qui s’intéressent aux nouvelles façons de créer des produits, pas seulement numériques et qui cherchent de nouvelles formes d’organisation du travail en équipe. Les non informaticiens et les padawans de l’agilité sont les bienvenus. On peut venir seul ou en groupe déjà constitué venant d’une entreprise. Tout est fait pour faciliter la communication et la coopération entre les participants.

{{< figure src="/images/photos/2018-06/raid9-12.jpg" title="" >}}
