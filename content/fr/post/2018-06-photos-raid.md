---
date: 2018-06-09
description: "Photos du 9 juin 2018"
aliases : "/raid9.html"
featured_image: "/images/photos/2018-06/raid9-3.jpg"
tags: ["souvenirs"]
title: "Raid Agile #9, juin 2018, les photos"
---

Les photos du raid agile de juin 2018 avec Laurent Morisseau. 


{{< figure src="/images/photos/2018-06/raid9-1.jpg" caption="User Story Mapping" >}}
{{< figure src="/images/photos/2018-06/raid9-2.jpg" caption="Le dernier avant longtemps" >}}
{{< figure src="/images/photos/2018-06/raid9-3.jpg" caption="L'appel de la nature" >}}
{{< figure src="/images/photos/2018-06/raid9-4.jpg" caption="Tension sur le boulodrome" >}}
{{< figure src="/images/photos/2018-06/raid9-5.jpg" caption="beaucoup de bouteilles" >}}
{{< figure src="/images/photos/2018-06/raid9-6.jpg" caption="Réveil du corps" >}}
{{< figure src="/images/photos/2018-06/raid9-7.jpg" caption="Cheveux blancs et barbes" >}}
{{< figure src="/images/photos/2018-06/raid9-8.jpg" caption="Here comes the sun" >}}
{{< figure src="/images/photos/2018-06/raid9-9.jpg" caption="Avec Laurent Morisseau" >}}
{{< figure src="/images/photos/2018-06/raid9-10.jpg" caption="À la baguette" >}}
{{< figure src="/images/photos/2018-06/raid9-11.jpg" caption="User story mapping et impact mapping" >}}
{{< figure src="/images/photos/2018-06/raid9-12.jpg" caption="Repas, soirée" >}}
{{< figure src="/images/photos/2018-06/raid9-13.jpg" caption="Atelier" >}}
{{< figure src="/images/photos/2018-06/raid9-14.jpg" caption="Born to be wild" >}}
{{< figure src="/images/photos/2018-06/raid9-15.jpg" caption="In uncertainty we trust" >}}
{{< figure src="/images/photos/2018-06/raid9-16.jpg" caption="Balade et rando" >}}
{{< figure src="/images/photos/2018-06/raid9-17.jpg" caption="En haut du Mont Brion" >}}
{{< figure src="/images/photos/2018-06/raid9-18.jpg" caption="Bâton d'hélium" >}}
