---
date: 2017-05-01
description: "Photos du raid de mai 2017"
aliases : "/raid7.html"
featured_image: "/images/photos/2017-05/raid7-10.jpg"
tags: ["souvenirs"]
title: "Raid Agile #7, mai 2017, les photos"
---

Les photos du raid agile de mai 2017.


{{< figure src="/images/photos/2017-05/raid7-1.jpg" caption="Session sur la terrasse" >}}
{{< figure src="/images/photos/2017-05/raid7-2.jpg" caption="Agile Fluency" >}}
{{< figure src="/images/photos/2017-05/raid7-3.jpg" caption="À nous le Mont Brion" >}}
{{< figure src="/images/photos/2017-05/raid7-4.jpg" caption="Retour au mas du canton" >}}
{{< figure src="/images/photos/2017-05/raid7-5.jpg" caption="Hein ? " >}}
{{< figure src="/images/photos/2017-05/raid7-6.jpg" caption="Contenu" >}}
{{< figure src="/images/photos/2017-05/raid7-7.jpg" caption="Session studieuse" >}}
{{< figure src="/images/photos/2017-05/raid7-8.jpg" caption="Session" >}}
{{< figure src="/images/photos/2017-05/raid7-9.jpg" caption="Claude Aubry" >}}
{{< figure src="/images/photos/2017-05/raid7-10.jpg" caption="Ouverture" >}}
{{< figure src="/images/photos/2017-05/raid7-11.jpg" caption="Le duo Ratatouille (cuisine)" >}}
{{< figure src="/images/photos/2017-05/raid7-12.jpg" caption="Session en balade" >}}
