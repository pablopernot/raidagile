---
date: 2022-06-03
description: "L'équipe du raid 2023"
featured_image: "/images/photos/2018-06/raid9-13.jpg"
tags: ["préparation"]
title: "L'équipe du raid 2023"
---

Pour le Raid Agile 2023, une nouvelle équipe ?

<!--more-->

On ne change pas une équipe qui gagne. Le Raid Agile 2023 sera toujours animé par Pablo et Claudio. Comme c'est le dernier Raid de Claudio, il y aura aussi Dragos qui prendra le relais.

Nous serons donc trois !

{{< figure src="/images/animateurs2022.jpg" caption="L'équipe du raid 2023 : Claude Aubry, Pablo Pernot, Dragos Dreptate (du plus gentil au moins gentil)" >}}

### Dragos

À l'instar du petit garçon dans "les habits neufs de l'empereur", Dragos aide les organisations à se poser les bonnes questions et à trouver des réponses dans leur chemin de transformation qui passe par une prise de conscience de l'écart entre ce que les organisations pensent être, ce qu'elles sont vraiment et ce qu'elles veulent devenir. Sa mission: bâtisseur de possible, bâtisseur de demain. Sa raison d'être: donner du sens et aider les gens à trouver la joie et la fierté dans ce qu'ils accomplissent.

Il est le co-fondateur de la [school of product](https://schoolofpo.com/) (avec pablo)

en savoir plus [Dragos Dreptate](https://www.linkedin.com/in/dragosdreptate/)

### Pablo

Après une maîtrise sur les Monty Python, un DEA sur le non-sens et l’absurde, l’entame d’un doctorat sur les comiques cinématographiques, il paraît normal que je me sois lancé dans le management et la conduite du changement, c’est -finalement- une suite logique. Actuellement considéré un acteur expérimenté en management organisationnel (holacratie, sociocratie, agile, lean, kanban), j’accompagne de grands groupes et leurs dirigeants dans leurs trajectoires.

en savoir plus : [pablopernot.fr](https://pablopernot.fr)


### Claudio

Auparavant développeur, responsable d’équipes puis consultant en ingénierie logiciel et systèmes, il se consacre entièrement à l’agilité depuis 2005. Avec son blog Scrum, Agilité & rock’n’roll, son implication dans la communauté et ses interventions dans les conférences il a contribué à la diffusion des méthodes agiles en France.

Il a formé plus de 1 000 personnes à l’agilité et à Scrum et a conseillé plus d’une centaine d’organisations dans leur transformation agile.

Il est l’auteur du livre L’art de devenir une équipe agile, illustré par Etienne Appert et de la référence sur Scrum, la 6e édition parue en janvier 2022 a pour sous-titre **Un outil convivial pour une agilité radicale**.

en savoir plus : [Scrum, agilité & rock'n roll](https://www.aubryconseil.com/)
