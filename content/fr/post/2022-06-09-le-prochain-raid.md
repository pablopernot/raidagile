---
title: "Le Raid Agile revient !"
date: 2022-06-09T20:24:00+02:00
draft: false
---

Le dernier Raid Agile a eu lieu en juin 2018. En quatre ans, nous avions organisé dix raids. Cela demande beaucoup d'énergie et nous avons préféré la consacrer chacun à d'autres activités. Du business, des livres, des collectifs, des fusions, des réflexions, des enfants et petits enfants.

Et puis…

<!--more-->

> Il y aura un prochain Raid Agile, début 2023.

Voici pourquoi :

(Claude)Au printemps 2022, je me suis dit qu'il était temps pour moi d'arrêter mes activités professionnelles. Je suis consultant indépendant depuis 1994, en profession libérale. Chaque année, je remplis une déclaration 2035. J'ai beau avoir de moins en moins de choses à y mettre (recettes, dépenses), mon aversion pour la bureaucratie augmente avec les ans. En avril, je me suis donc dit que c'était la dernière fois (l'avant-dernière, en fait).

Une fois cette décision [^1] prise, je me suis dit qu'il me fallait finir en beauté ces 28 années consacrées à la formation et au conseil, dont les 17 dernières dédiées à Scrum et l'agilité.

[^1]: la décision d'arrêter en profession libérale n'implique pas d'arrêter de m'intéresser à l'agilité, d'écrire, de réfléchir, de coopérer et de parler sur le sujet, ni même de continuer en auto-entrepreneur.

J'aurais pu proposer une _master class_ basée sur mon dernier livre (le Scrum édition 6), comme le font certains auteurs. C'est Jean-Pascal[^2] — qui a souvent de bonnes idées — qui m'a suggéré de refaire un Raid Agile.

[^2]: Jean-Pascal est un raider, participant au Raid Agile #8, lire [son témoignage](/post/temoignages/)

Bon Dieu, mais c'est bien sûr ! Finir trente cinq ans de formations par un Raid, qui représente pour moi la quintessence de la transmission, voilà ce dont j'avais envie. Dans le même lieu - les Cévennes - et bien sûr, avec Pablo.

Pour lire plus de Claude sur le Raid Agile : [ses articles](https://www.aubryconseil.com/tags/raid-agile/)

(Pablo)

[Il explique pourquoi sur son blog](https://pablopernot.fr/2022/09/raid-agile-2022/).
